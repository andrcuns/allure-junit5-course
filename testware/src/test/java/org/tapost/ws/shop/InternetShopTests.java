package org.tapost.ws.shop;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tapost.ws.sut.Account;
import org.tapost.ws.sut.InternetShop;
import org.tapost.ws.sut.Item;
import org.tapost.ws.sut.ShoppingCart;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.BooleanSupplier;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class InternetShopTests {
    private static final Logger log = LoggerFactory.getLogger("InternetShopTest");

    @Nested
    class ShopFunctionsTest {
        Account account;

        @BeforeEach
        void login(TestInfo testInfo) {
            String username = "user1";
            String password = "pwd";
            log.info("Before setup of {}", testInfo.getTestMethod().get().getName());
            account = InternetShop.login(username, password);
        }

        @AfterEach
        void logout() {
            log.info("Loggin out user");
            InternetShop.logout();
        }

        @Tag("Primary")
        @Test
        @DisplayName("TopUp")
        void topUp() {
            BigDecimal topUpBalance = new BigDecimal(1.0);
            log.info("Balance for top up {}", topUpBalance);

            InternetShop.topUpBalance(topUpBalance);

            BigDecimal balanceInShop = InternetShop.balance();
            Assertions.assertTrue(topUpBalance.equals(balanceInShop));
        }

        @Test
        @Tag("Secondary")
        @DisplayName("Search")
        void searchItems() {
            List<Item> found = InternetShop.searchItems("Ice Cream");

            log.info("Found items: {}", found);
            BooleanSupplier iceCreamItems = () -> found.stream().allMatch(item -> item.getName().equals("Ice Cream"));

            assertAll("Item",
                    () -> Assertions.assertTrue(iceCreamItems),
                    () -> Assertions.assertTrue(found.size() == 1)
            );
        }

        @Tag("Primary")
        @ParameterizedTest(name = "Cart: {index}")
        @CsvSource(value = {"Beer, 5", "Ice Cream, 10"})
        void shoppingCart(String itemToFind, @ConvertWith(StringToBigDecimal.class) BigDecimal price) {
            BigDecimal quantity = new BigDecimal(3);
            List<Item> found = InternetShop.searchItems(itemToFind);
            InternetShop.addToCart(found.get(0).getId(), quantity.intValue());

            ShoppingCart shoppingCart = InternetShop.viewShoppingCart();
            String itemName = shoppingCart.getItems()
                    .get(0)
                    .getItem()
                    .getName();

            assertAll("Shopping Cart",
                    () -> assertEquals(itemName, itemToFind),
                    () -> assertEquals(shoppingCart.getTotal(), price.multiply(quantity)),
                    () -> assertEquals(shoppingCart.getItems().size(), 1)
            );
        }

    }

    @Nested
    class LoginTest {

        @AfterEach
        void logOut() {
            InternetShop.logout();
        }

        @Tag("Account")
        @ParameterizedTest(name = "Account: {index}")
        @CsvFileSource(resources = "/users.csv")
        void testAdminAccount(String username, String password, String firstName, String lastName, String role) {
            Account account = InternetShop.login(username, password);
            assertAll("Admin account detauls",
                    () -> assertEquals(account.getFirstName(), firstName),
                    () -> assertEquals(account.getLastName(), lastName),
                    () -> assertEquals(account.getRole(), role)
            );
        }
    }
}
