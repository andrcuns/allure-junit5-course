package org.tapost.ws.shop;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectClasses(InternetShopTests.class)
@IncludeTags("Primary")
public class PrimaryTestPlan {
}
