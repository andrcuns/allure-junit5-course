package org.tapost.ws.shop;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

import java.math.BigDecimal;

public class StringToBigDecimal extends SimpleArgumentConverter {
    @Override
    protected Object convert(Object source, Class<?> targetType) throws ArgumentConversionException {
        return new BigDecimal((String) source);
    }
}
